# Entendendo o salvarTele.xml

Possui os dados referente a um pedido de tele entrega e dados de cadastro do cliente, desta forma é possivel atualizar os dados do cliente em uma única chamada.

##### Observe que:
 - O nodo **tele_item** deve ser repetido para cada produto a ser enviado.
 - A maioria dos nodos seguem essas três regras (acompanhe a tabela para saber mais sobre cada campo):
    1. Ao enviar um nodo vazio ou removê-lo do XML, fará com que o Midas utilize o valor padrão de um cliente já cadastrado;
    2. A tag **[EMPTY]** dentro do nodo **tele_cliente** apaga o valor no cadastro do cliente;
    3. A tag **[EMPTY]** dentro do nodo **tele_pedido** apaga o valor somente no pedido atual, sem alterar o valor no cadastro do cliente.

##### Legenda
    *  = campo obrigatório.
    ** = campo obrigatório em novo cadastro em pedido com entregador.


| Nodo                 | Tipo    | Tamanho  | Regras           | Função       |
|:---------------------|:-------:|:--------:|:----------------:|:-------------|
| **TELE_CLIENTE** |||||
| fone_principal       | numeric | 11       | *                |Telefone principal, utilizado como chave para cadastrar/editar clientes. **DDD é obrigatório** |
| nome                 | string  | 60       | **               | Nome completo ou razão social do cliente. |
| fantasia             | string  | 20       |                  | Apelido ou fantasia do cliente. |
| endereco             | string  | 50       | **               | Endereço do cliente. |
| endereco_numero      | string  | 10       | **               | Número do endereço do cliente. |
| bairro               | string  | 25       | **               | Bairro onde o cliente reside. |
| cidade               | string  | 25       | **               | Cidade onde o cliente reside. |
| uf                   | string  | 2        |                  | Estado onde o cliente reside. |
| cep                  | string  | 9        | **               | CEP do cliente. |
| proximidades         | string  | 120      | [EMPTY]          | Descrição do endereço informada pelo cliente. |
| email                | string  | 50       | [EMPTY]          | E-mail do cliente. |
| fone                 | string  | 15       | [EMPTY]          | Telefone secundário do cliente. |
| fisica_juridica      | string  | 1        |                  | Identificação de natureza do cliente. F = física J = jurídica |
| cpf_cpnpj            | string  | 20       |                  | CPF ou CPNJ do cliente. |
| rg_inscricao         | string  | 20       |                  | RG do cliente. |
| nascicmeto           | string  | 10       |                  | Data de nascimento do cliente. Formato: dd/mm/yyyy |
| sexo                 | string  | 1        |                  | Gênero do cliente. |
| observacao           | string  |          | [EMPTY]          | Uma observação sobre o cliente. |
| -- |||||
| **TELE_PEDIDO** |||||
| retirar              | string  | 1        | *                | Flag indicando se deve ser entregue ou cliente irá buscar no estabelecimento, eliminando o valor da entrega. S = Retirar no estabelecimento N = Entrega por motoboy |
| entrega_valor        | numeric | 12,2     |                  | Valor da entrega do pedido de acordo com nodo **regiao**. |
| entrega_data         | string  | 10       |                  | Data da entrega. Formato: dd/mm/yyyy |
| entrega_hora         | string  | 5        |                  | Horário da entrega. Formato: hh:mm |
| entrega_endereco     | string  | 50       |                  | Quando preenchido, o sistema ignorará o endereço de cadastro. Exemplo de uso: quando o cliente está na casa de um amigo.|
| entrega_bairro       | string  | 25       |                  | Mesma função do nodo **entrega_endereco**. |
| entrega_cidade       | string  | 25       |                  | Mesma função do nodo **entrega_endereco**. |
| entrega_uf           | string  | 2        |                  | Mesma função do nodo **entrega_endereco**. |
| entrega_cep          | string  | 9        |                  | Mesma função do nodo **entrega_endereco**. |
| entrega_proximidades | string  | 120      | [EMPTY]          | Mesma função do nodo **entrega_endereco**. |
| moeda                | numeric | 3        | *                | Código de forma de pagamento cadastradas no Midas. |
| valor_cobrado        | numeric | 12,2     |                  | Valor total do pedido (caso esteja em branco, o "valor_pago" é utilizado). |
| valor_pago           | numeric | 12,2     | *                | Valor pago pelo cliente. |
| voucher1_codigo      | numeric |          |                  | Código do cupom voucher. |
| voucher1_valor       | numeric | 12,2     |                  | Valor do cupom voucher.  |
| voucher1_alvo        | string  | 10       |                  | Alvo de desconto voucher.|
| voucher2_codigo      | numeric |          |                  | Código do cupom voucher. |
| voucher2_valor       | numeric | 12,2     |                  | Valor do cupom voucher.  |
| voucher2_alvo        | string  | 10       |                  | Alvo de desconto voucher.|
| voucher3_codigo      | numeric |          |                  | Código do cupom voucher. |
| voucher3_valor       | numeric | 12,2     |                  | Valor do cupom voucher.  |
| voucher3_alvo        | string  | 10       |                  | Alvo de desconto voucher.|
| market_place         | string  | 20       |                  | Identificação do market place/portal de pedidos.|
| referencia           | string  | 20       |                  | Código de identificação do pedido dentro do market place|portal de pedidos.|
| id_rastreio          | string  | 20       |                  | Código para utilização na aplicação dos entregadores para disponibilizar rastreio do pedido ao consumidor.|
| observacao           | string  | 250      | [EMPTY]          | Quando preenchido, o sistema irá ignorar o valor do **observacao** no nodo **tele_cliente**. |
| tags                 | string  | 250       |                  | Campo livre para customizações específicas, caso o xml não atenda alguma peculiaridade da aplicação que o consome.|
| -- |||||
| **TELE_ITEM** |||||
| codigo               | numeric | 13       | *                | Código do produto. |
| quantidade           | numeric | 11,3     | *                | Quantidade do produto. |
| preco                | numeric | 12,2     |                  | Preço do produto praticado dentro do market place/portal de pedidos. |
| sabor1               | numeric | 3        |                  | Código do sabor/adicional acrescentado ao produto. |
| complemento1         | string  | 100      |                  | Complemento do sabor 1 |
| sabor2               | numeric | 3        |                  | Código do segundo sabor/adicional acrescentado ao produto. |
| complemento2         | string  | 100      |                  | Complemento do sabor 2 |
| sabor3               | numeric | 3        |                  | Código do terceiro sabor/adicional acrescentado ao produto. |
| complemento3         | string  | 100      |                  | Complemento do sabor 3 |
| sabor4               | numeric | 3        |                  | Código do quarto sabor/adicional acrescentado ao produto. |
| complemento4         | string  | 100      |                  | Complemento do sabor 4 |
| sabor5               | numeric | 3        |                  | Código do quinto sabor/adicional acrescentado ao produto. |
| complemento5         | string  | 100      |                  | Complemento do sabor 5 |
| observacao           | string  | 200      |                  | Descrição/observação sobre o produto. |
