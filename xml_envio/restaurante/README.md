# Entendendo o salvarMesa.xml

Possui os dados referente a mesa em atendimento junto com os dados do(s) produto(s) pedido(s).

##### Observe que:
 - O nodo **CURTMPMESA** deve ser repetido para cada produto a ser enviado.
 - Os produtos já enviados em um momento anterior também devem estar no XML com seus dados originais e com o nodo **"emitido"** contendo o valor **"1"**.

| Nodo         | Tipo    | Obrigatório | Função       |
|:-------------|:-------:|:-----------:|:-------------|
| ambiente     | string  | *           | Ambiente a qual a mesa pertence. B = balcão I = Interno E = Externo |
| mesa         | int     | *           | Número da mesa em atendimento.  |
| nomecliente  | string  |             | Descrição/Observação sobre mesa ou cliente. |
| qtdpessoas   | int     |             | Número de pessoas na mesa. |
| data         | string  | *           | Horário de adição do produto. Formato: dd/mm/yyyy |
| hora         | string  | *           | Horário de adição do produto. Formato: hh:mm:ss |
| funcionario  | int     | *           | Código do atendente que adicionou o produto. |
| produto      | long    | *           | Código do produto adicionado. |
| complementos | string  |             | Uma observação sobre o produto. Ex.: Com gelo. |
| sab1         | int     |             | Código de um sabor/adicional acrescentado ao produto. |
| cop1         | string  |             | Uma observação sobre o **sab1**. |
| sab2         | int     |             | Código de um segundo sabor/adicional acrescentado ao produto. |
| cop2         | string  |             | Uma observação sobre o **sab2**. |
| sab3         | int     |             | Código de um terceiro sabor/adicional acrescentado ao produto. |
| cop3         | string  |             | Uma observação sobre o **sab3**. |
| sab4         | int     |             | Código de um quarto sabor/adicional acrescentado ao produto. |
| cop4         | string  |             | Uma observação sobre o **sab4**. |
| sab5         | int     |             | Código de um quinto sabor/adicional acrescentado ao produto. |
| cop5         | string  |             | Uma observação sobre o **sab5**. |
| quantidade   | string  | *           | Quantidade do produto. |
| preco        | float   | *           | Valor do produto. O valor pode variar de acordo com algums configurações, por exemplo, o uso de sabores/adicionais. |
| estacao      | string  | *           | Nome do dispositivo de onde o produto foi adicionado. |
| usuario      | string  | *           | Usuário do sistema que adicionou o produto. |
| emitido      | int     |             | Flag utilizada para indicar se produto já foi enviado em um momento anterior. 1 = verdadeiro<br> 0 = falso |
